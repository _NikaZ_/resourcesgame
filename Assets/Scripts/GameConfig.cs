using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGameConfig", menuName = "Game/Game Config")]
public class GameConfig : ScriptableObject
{
    [Header("Resources")]
    public int peopleCount = 10;
    public int foodCount = 5;
    public int woodCount = 5;
    public int stoneCount = 0;
    public int goldCount = 0;

    [Header("Production levels")]
    public int humanProdLvl;
    public int foodProdLvl;
    public int woodProdLvl;
    public int stoneProdLvl;
    public int goldProdLvl;
}
