using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Production
{
    Dictionary<ResourceType, int> productionLevels;

    public Production(GameConfig config)
    {
        productionLevels = new Dictionary<ResourceType, int>()
        {
            [ResourceType.FOOD] = config.foodProdLvl,
            [ResourceType.GOLD] = config.goldProdLvl,
            [ResourceType.HUMAN] = config.humanProdLvl,
            [ResourceType.STONE] = config.stoneProdLvl,
            [ResourceType.WOOD] = config.woodProdLvl
        };
    }

    public void UpgradeProduction(ResourceType type)
    {
        if (!productionLevels.ContainsKey(type))
        {
            throw new System.IO.InvalidDataException();
        }

        productionLevels[type] += 1;
    }

    public int GetLevel(ResourceType type)
    {
        if (!productionLevels.ContainsKey(type))
        {
            throw new System.IO.InvalidDataException();
        }

        return productionLevels[type];
    }
}
